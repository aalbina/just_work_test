from django.contrib import admin

from .models import Audio, Content, Page, Text, Video


@admin.register(Audio)
class AudioAdmin(admin.ModelAdmin):
    search_fields = ['title__startswith']
    readonly_fields = ('counter', )


class AudioInline(admin.TabularInline):
    model = Audio
    readonly_fields = ('counter', )


@admin.register(Video)
class VideoAdmin(admin.ModelAdmin):
    search_fields = ['title__startswith']
    readonly_fields = ('counter', )


class VideoInline(admin.TabularInline):
    model = Video
    readonly_fields = ('counter', )


@admin.register(Text)
class TextAdmin(admin.ModelAdmin):
    search_fields = ['title__startswith']
    readonly_fields = ('counter', )


class TextInline(admin.TabularInline):
    model = Text
    readonly_fields = ('counter', )


class ContentInline(admin.TabularInline):
    model = Page.content.through


@admin.register(Content)
class ContentAdmin(admin.ModelAdmin):
    exclude = ('title', 'counter')

    def has_add_permission(self, request, obj=None):
        return False

    def get_inline_instances(self, request, obj=None):
        if getattr(obj, 'audio', None):
            self.inlines = [AudioInline, ]
        elif getattr(obj, 'video', None):
            self.inlines = [VideoInline, ]
        elif getattr(obj, 'text', None):
            self.inlines = [TextInline, ]
        return super().get_inline_instances(request, obj)


@admin.register(Page)
class PageAdmin(admin.ModelAdmin):
    search_fields = ['title__startswith']
    inlines = [ContentInline]
    exclude = ('content', )
