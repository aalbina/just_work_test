from django.db import models


class Content(models.Model):
    title = models.CharField(max_length=200)
    counter = models.IntegerField(default=0)

    def __str__(self):
        return self.title


class Audio(Content):
    bit_rate = models.IntegerField()

    def __str__(self):
        return self.title


class Text(Content):
    contents = models.TextField()

    def __str__(self):
        return self.title


class Video(Content):
    video_link = models.CharField(max_length=200)
    subtitles_link = models.CharField(max_length=200, blank=True, null=True)

    def __str__(self):
        return self.title


class Page(models.Model):
    title = models.CharField(max_length=200)
    content = models.ManyToManyField(
        Content, through='PageContent', blank=True)

    def __str__(self):
        return self.title


class PageContent(models.Model):
    page = models.ForeignKey(Page, on_delete=models.CASCADE)
    content = models.ForeignKey(Content, on_delete=models.CASCADE)
    order = models.IntegerField(default=0)

    class Meta:
        ordering = ('order', )
