from django.test import TestCase
from model_mommy import mommy

from .models import Audio, Content, Page, PageContent, Text, Video


class AudioTestMommy(TestCase):
    def test_audio_creation_mommy(self):
        audio = mommy.make(Audio)
        self.assertTrue(isinstance(audio, Audio))
        self.assertEqual(audio.__str__(), audio.title)


class ContentTestMommy(TestCase):
    def test_content_creation_mommy(self):
        content = mommy.make(Content)
        self.assertTrue(isinstance(content, Content))
        self.assertEqual(content.__str__(), content.title)


class PageTestMommy(TestCase):
    def test_page_creation_mommy(self):
        page = mommy.make(Page)
        self.assertTrue(isinstance(page, Page))
        self.assertEqual(page.__str__(), page.title)


class PageContentTestMommy(TestCase):
    def test_page_content_creation_mommy(self):
        page_content = mommy.make(PageContent)
        self.assertTrue(isinstance(page_content, PageContent))


class TextTestMommy(TestCase):
    def test_text_creation_mommy(self):
        text = mommy.make(Text)
        self.assertTrue(isinstance(text, Text))
        self.assertEqual(text.__str__(), text.title)


class VideoTestMommy(TestCase):
    def test_video_creation_mommy(self):
        video = mommy.make(Video)
        self.assertTrue(isinstance(video, Video))
        self.assertEqual(video.__str__(), video.title)


class ContentOrderTest(TestCase):
    page = None
    first_content = None
    second_content = None

    def setUp(self):
        self.page = mommy.make(Page)
        self.first_content = mommy.make(Content)
        self.second_content = mommy.make(Content)

        PageContent.objects.create(
            page=self.page, content=self.first_content, order=1)
        PageContent.objects.create(
            page=self.page, content=self.second_content, order=2)

    def test_ordering_content(self):
        self.assertEqual(self.page.pagecontent_set.first().content,
                         self.first_content)

        first_pagecontent = self.page.pagecontent_set.first()
        first_pagecontent.order = 3
        first_pagecontent.save()

        self.assertEqual(self.page.pagecontent_set.first().content,
                         self.second_content)
