# Simple django app
## Requirements
Docker + docker‑compose 

## How to start
`docker‑compose up runserver` - starts server with Django app and seeds initial data

`docker‑compose up autotests` - starts all auto-tests for the app


## API call samples

### List pages
Returns list of Pages. 

Pagination is supported. By default every page consists of 5 objects. 
The response has the following fields:
* `count` - number of pages in the system
* `next` - link for request for the next bunch of Pages
* `previous` = link for request for the previous bunch of Pages
* `results` - array with the Page objects.

Page object has the following fields:
* `title` - page title
* `url` - url to the page's info page

##### Method description
* **URL**

    `/api/pages/`

* **Method**

    `GET`

* **URL Params**

    **Required:**

    `None`
    
    **Optional:**
    
    `limit=[integer]` - max number of pages in the list expected
    
    `offset=[integer]` - offset from the beginning of the pages list

* **Data Params**
    
    `None`

* **Success response**

    * **Code:** 200 <br />
    **Content:** 
    ```
    {
        "count": 6,
        "next": "http://0.0.0.0:8000/api/pages/?limit=5&offset=5",
        "previous": null,
        "results": [
            {
                "url": "http://0.0.0.0:8000/api/pages/1",
                "title": "Adele"
            },
            {
                "url": "http://0.0.0.0:8000/api/pages/2",
                "title": "Drake"
            },
            {
                "url": "http://0.0.0.0:8000/api/pages/3",
                "title": "Stromae"
            },
            {
                "url": "http://0.0.0.0:8000/api/pages/4",
                "title": "TherrMaitz"
            },
            {
                "url": "http://0.0.0.0:8000/api/pages/5",
                "title": "John"
            }
        ]
    }
    ```
* **Sample call**
    ```javascript
        $.ajax({
          url: "/api/pages",
          dataType: "json",
          type : "GET",
          success : function(r) {
            console.log(r);
          }
        });
    ```


### Page info
Returns the Page info. 

Page object has the following fields:
* `title` - page title
* `content` - array of content objects on the page

Content object could be of three types. Each Content object the following common fields:
* `id` - id of the object in the system
* `title` - title of the object
* `counter` - number of views of this object
Also every type of the content has it's own special fields:
* Audio:
    * `bit_rate` - bit rate of the song
* Video:
    * `video_link` - link to the video file
    * `subtitles_link` - link to the subtitles file
* Text:
    * `contents` - text itself

##### Method description
* **URL**

    `/api/pages/:id`

* **Method**

    `GET`

* **URL Params**

    **Required:**

    `id=[integer]` - id of the Page
    
    **Optional:**
    
    `None`

* **Data Params**
    
    `None`

* **Success response**

    * **Code:** 200 <br />
    **Content:** 
    ```
    {
        "title": "Drake",
        "content": [
            {
                "id": 7,
                "title": "Drake's video",
                "counter": 7,
                "video_link": "https://youtu.be/uxpDa-c-4Mc",
                "subtitles_link": "http://www.rentanadviser.com/en/subtitles/getsubtitle.aspx?artist=Drake&song=Hotline%20Bling"
            },
            {
                "id": 6,
                "title": "Drake's audio",
                "counter": 7,
                "bit_rate": 118
            }
        ]
    }
    ```
* **Error response**

    * **Code:** 404 <br />
    **Content:** 
    ```
    {
        "detail": "Not found."
    }
    ```
* **Sample call**
    ```javascript
        $.ajax({
          url: "/api/pages/2",
          dataType: "json",
          type : "GET",
          success : function(r) {
            console.log(r);
          }
        });
    ```
