import json

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from api.serializers import PageSerializer
from core.models import Page


class PageViewTestCase(APITestCase):
    fixtures = ['initial']
    simple_page_pk = 4
    not_existing_page_pk = 100

    def test_api_can_retrieve_pages(self):
        url = reverse('page-list')
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_api_can_retrieve_page_info(self):
        page = Page.objects.get(pk=self.simple_page_pk)

        url = reverse('page-detail', kwargs={"pk": self.simple_page_pk})
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content),
                         PageSerializer(page).data)

    def test_api_can_throw_not_found(self):
        url = reverse('page-detail', kwargs={"pk": self.not_existing_page_pk})
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(json.loads(response.content),
                         {"detail": "Not found."})
