from rest_framework.generics import ListAPIView, RetrieveAPIView

from core.models import Page
from .serializers import PageSerializer, PageListSerializer
from .tasks import update_content_counter


class PageListView(ListAPIView):
    """
    API endpoint that retrieves all pages.
    """
    queryset = Page.objects.all()
    serializer_class = PageListSerializer


class PageRetrieveView(RetrieveAPIView):
    """
    API endpoint that retrieves page info.
    """
    queryset = Page.objects.all()
    serializer_class = PageSerializer

    def get_object(self):
        page = super().get_object()

        update_content_counter.delay(page.id)
        return page
