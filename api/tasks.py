from django.db.models import F

from core.models import Page
from just_work_test.celery import app


@app.task
def update_content_counter(page_id):
    try:
        Page.objects.get(pk=page_id).content.update(counter=F('counter') + 1)
    except Page.DoesNotExist:
        pass
