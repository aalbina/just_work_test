from rest_framework import serializers

from core.models import Audio, Content, Page, PageContent, Text, Video


class AudioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Audio
        fields = '__all__'
        depth = 2


class PageContentSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        return ContentSerializer(instance=instance.content).data

    class Meta:
        model = PageContent


class ContentSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        if getattr(instance, 'audio', None):
            return AudioSerializer(instance=instance.audio).data
        elif getattr(instance, 'text', None):
            return TextSerializer(instance=instance.text).data
        elif getattr(instance, 'video', None):
            return VideoSerializer(instance=instance.video).data
        else:
            return super().to_representation(instance=instance)

    class Meta:
        model = Content
        fields = ('title', 'counter')


class PageListSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Page
        fields = ('url', 'title')


class PageSerializer(serializers.ModelSerializer):
    content = PageContentSerializer(
        source="pagecontent_set", many=True, read_only=True)

    class Meta:
        model = Page
        fields = ('title', 'content')
        depth = 1


class TextSerializer(serializers.ModelSerializer):
    class Meta:
        model = Text
        fields = '__all__'
        depth = 2


class VideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Video
        fields = '__all__'
        depth = 2
