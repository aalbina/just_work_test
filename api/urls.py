from django.urls import path, include

from .views import PageListView, PageRetrieveView

urlpatterns = [
    path('pages/', PageListView.as_view(), name='page-list'),
    path('pages/<int:pk>', PageRetrieveView.as_view(), name='page-detail'),
    path('auth/', include('rest_framework.urls'))
]
